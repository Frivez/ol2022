﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace mysqltest.Models
{
    public class Email
    {
        owldbEntities01 owldb = new owldbEntities01();
        public void Execute()
        {
            var key = "";
            key = owldb.questions.Where(a => a.question_id == 1).FirstOrDefault().question;

            //var apiKey = Environment.GetEnvironmentVariable("NAME_OF_THE_ENVIRONMENT_VARIABLE_FOR_YOUR_SENDGRID_KEY");
            var client = new SendGridClient(key);
            var from = new EmailAddress("owllearningcr@gmail.com", "Owl Learning");
            var subject = "Sending with SendGrid is Fun";
            var to = new EmailAddress("frr9724@gmail.com", "Francisco");
            var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = client.SendEmailAsync(msg);
        }
    }
}