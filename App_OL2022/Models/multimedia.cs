//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mysqltest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class multimedia
    {
        public int idMultimedia { get; set; }

        [Required(ErrorMessage = "* Ingrese la descripci�n del video")]
        public string Description { get; set; }

        [Required(ErrorMessage = "* Ingrese el tema del video")]
        public string Tema { get; set; }

        [Required(ErrorMessage = "* Ingrese la direci�n URL del video")]
        public string URL { get; set; }
        public int Estado { get; set; }
    }
}
