﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mysqltest.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class owldbEntities01 : DbContext
    {

        public owldbEntities01()
            : base("name=owldbEntities01")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<course_assignment> course_assignment { get; set; }
        public virtual DbSet<course_schedule> course_schedule { get; set; }
        public virtual DbSet<course_type> course_type { get; set; }
        public virtual DbSet<courses> courses { get; set; }
        public virtual DbSet<logs> logs { get; set; }
        public virtual DbSet<module_vt> module_vt { get; set; }
        public virtual DbSet<modules> modules { get; set; }
        public virtual DbSet<pass_verif_code> pass_verif_code { get; set; }
        public virtual DbSet<question_vt> question_vt { get; set; }
        public virtual DbSet<questions> questions { get; set; }
        public virtual DbSet<role_user> role_user { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<schedules> schedules { get; set; }
        public virtual DbSet<test_type> test_type { get; set; }
        public virtual DbSet<users> users { get; set; }
        public virtual DbSet<virtual_tests> virtual_tests { get; set; }
        public virtual DbSet<multimedia> multimedia { get; set; }
        public virtual DbSet<FileControl> FileControl { get; set; }
        public virtual DbSet<answer_vt> answer_vt { get; set; }
        public virtual DbSet<vt_scored_record> vt_scored_record { get; set; }
        public virtual DbSet<FC_Module> FC_Module { get; set; }
        public virtual DbSet<Mod_Content> Mod_Content { get; set; }
        public virtual DbSet<Multi_Modulo> Multi_Modulo { get; set; }
    
        public virtual int addUser(string dni, string first_name, string last_name, string phone_number, string email, string password)
        {
            var dniParameter = dni != null ?
                new ObjectParameter("dni", dni) :
                new ObjectParameter("dni", typeof(string));
    
            var first_nameParameter = first_name != null ?
                new ObjectParameter("first_name", first_name) :
                new ObjectParameter("first_name", typeof(string));
    
            var last_nameParameter = last_name != null ?
                new ObjectParameter("last_name", last_name) :
                new ObjectParameter("last_name", typeof(string));
    
            var phone_numberParameter = phone_number != null ?
                new ObjectParameter("phone_number", phone_number) :
                new ObjectParameter("phone_number", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("email", email) :
                new ObjectParameter("email", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("password", password) :
                new ObjectParameter("password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("addUser", dniParameter, first_nameParameter, last_nameParameter, phone_numberParameter, emailParameter, passwordParameter);
        }
    
        public virtual int asingStudentRole(Nullable<int> id_user)
        {
            var id_userParameter = id_user.HasValue ?
                new ObjectParameter("id_user", id_user) :
                new ObjectParameter("id_user", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("asingStudentRole", id_userParameter);
        }
    
        public virtual int deleteUser(Nullable<int> id_user)
        {
            var id_userParameter = id_user.HasValue ?
                new ObjectParameter("id_user", id_user) :
                new ObjectParameter("id_user", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("deleteUser", id_userParameter);
        }
    
        public virtual int generateVerificationCode(string user_email)
        {
            var user_emailParameter = user_email != null ?
                new ObjectParameter("user_email", user_email) :
                new ObjectParameter("user_email", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("generateVerificationCode", user_emailParameter);
        }
    
        public virtual ObjectResult<getSchedules_Result> getSchedules()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getSchedules_Result>("getSchedules");
        }
    
        public virtual ObjectResult<getTypes_Result> getTypes()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getTypes_Result>("getTypes");
        }
    
        public virtual ObjectResult<getUser_Result> getUser(Nullable<int> id_user)
        {
            var id_userParameter = id_user.HasValue ?
                new ObjectParameter("id_user", id_user) :
                new ObjectParameter("id_user", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getUser_Result>("getUser", id_userParameter);
        }
    
        public virtual ObjectResult<getUsers_Result> getUsers()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getUsers_Result>("getUsers");
        }
    
        public virtual int updateUser(Nullable<int> id_user, string dni, string first_name, string last_name, string phone_number, string email, string password)
        {
            var id_userParameter = id_user.HasValue ?
                new ObjectParameter("id_user", id_user) :
                new ObjectParameter("id_user", typeof(int));
    
            var dniParameter = dni != null ?
                new ObjectParameter("dni", dni) :
                new ObjectParameter("dni", typeof(string));
    
            var first_nameParameter = first_name != null ?
                new ObjectParameter("first_name", first_name) :
                new ObjectParameter("first_name", typeof(string));
    
            var last_nameParameter = last_name != null ?
                new ObjectParameter("last_name", last_name) :
                new ObjectParameter("last_name", typeof(string));
    
            var phone_numberParameter = phone_number != null ?
                new ObjectParameter("phone_number", phone_number) :
                new ObjectParameter("phone_number", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("email", email) :
                new ObjectParameter("email", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("password", password) :
                new ObjectParameter("password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("updateUser", id_userParameter, dniParameter, first_nameParameter, last_nameParameter, phone_numberParameter, emailParameter, passwordParameter);
        }
    
        public virtual ObjectResult<validateUser_Result> validateUser(string u_email, string u_pass)
        {
            var u_emailParameter = u_email != null ?
                new ObjectParameter("u_email", u_email) :
                new ObjectParameter("u_email", typeof(string));
    
            var u_passParameter = u_pass != null ?
                new ObjectParameter("u_pass", u_pass) :
                new ObjectParameter("u_pass", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<validateUser_Result>("validateUser", u_emailParameter, u_passParameter);
        }
    }
}
