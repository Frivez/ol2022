﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System.Linq;

namespace mysqltest.Models
{
    public class MailSender
    {
        owldbEntities01 owldb = new owldbEntities01();
        public void SendEmail(string emailTo, string name, string subject_1, string body)
        {
            var key = "";
            key = owldb.questions.Where(a => a.question_id == 1).FirstOrDefault().question;

            //var apiKey = Environment.GetEnvironmentVariable("API KEY");
            var client = new SendGridClient(key);
            var from = new EmailAddress("owllearningcr@gmail.com", "Owl Learning");
            var subject = subject_1;
            var to = new EmailAddress(emailTo, name);
            var plainTextContent = "Plain Text";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, body);
            var response = client.SendEmailAsync(msg);
        }

    }
}