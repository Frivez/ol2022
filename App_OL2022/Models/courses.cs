//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mysqltest.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class courses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public courses()
        {
            this.course_schedule = new HashSet<course_schedule>();
            this.modules = new HashSet<modules>();
            this.course_assignment = new HashSet<course_assignment>();
        }
        public int course_id { get; set; }

        [Required(ErrorMessage = "* Ingrese el nombre del curso")]
        public string name { get; set; }

        [Required(ErrorMessage = "* Ingrese una descripcion m�ximo de 150 caracteres")]
        [StringLength(150, MinimumLength = 10)]
        public string description { get; set; }

        [Required(ErrorMessage = "* Ingrese la duracion del curso")]
        [Range(2, 12, ErrorMessage = "* La duraci�n debe ser entre 2 y 12 meses.")]
        public int duration { get; set; }

        [Required(ErrorMessage = "* Ingrese el costo del curso")]
        [DataType(DataType.Currency)]
        [Range(1, 999999, ErrorMessage = "* S�lo valores positivos.")]
        public decimal cost { get; set; }

        [Required(ErrorMessage = "* Ingrese el tipo del curso")]
        public int type { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<course_schedule> course_schedule { get; set; }
        public virtual course_type course_type { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<modules> modules { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<course_assignment> course_assignment { get; set; }
    }
}
